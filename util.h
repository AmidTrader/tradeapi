#ifndef TRADEAPI_UTIL_H
#define TRADEAPI_UTIL_H

#include <QObject>

class QNetworkAccessManager;

typedef QObject* QObject_ptr;
typedef QNetworkAccessManager* QNetworkAccessManager_ptr;

void waitForSignal(QObject_ptr sender, const char *signal);

#endif // TRADEAPI_UTIL_H
