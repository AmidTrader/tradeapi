#ifndef TRADEAPI_H
#define TRADEAPI_H

#include <memory>

#include <QObject>

#include "request/base.h"
#include "response/bittrex/base.h"
#include "response/bittrex/type.h"
#include "response/bittrex/balance.h"

class QNetworkReply;
class QNetworkAccessManager;
#ifndef QT_NO_OPENSSL
class QSslError;
#endif

const double BITTREX_FEE = 0.0025;

class TradeApi: public QObject
{
    Q_OBJECT

    friend class ITradeApiRequestBase;
    friend class BittrexV11RequestProtected;

public:
    static TradeApi *instance();
    ~TradeApi();
    TradeApi(const TradeApi& other) = delete;
    TradeApi &operator=(const TradeApi& other) = delete;

    void setBittrexApiKey(const QString &key);
    void setBittrexApiSecretKey(const QString &key);

    void testRequest();
    BalanceCollection getBalances();
    OrderBookData getOrderBook(const QString &baseCurrency, const QString &currency);
    MarketsData getMarkets();
    MarketsSummaryData getMarketsSummary();
    BtcPriceInfo getBtcPrice();
    CurrencyCollection getCurrencies();
    OpenOrderCollection getOpenOrders(const QString &marketName = "" /* BTC-LTC */);
    OrderHistoryCollection getOrdersHistory(const QString &marketName = "" /* BTC-LTC */);
    NewOrderInfo getCreateNewOrder(const NewOrderInfo &info);

private:
    static TradeApi *_instance;
    QNetworkAccessManager_ptr _netManager = nullptr;
    QString _bittrexApiKey;
    QString _bittrexApiSecretKey;

    explicit TradeApi();

    QNetworkAccessManager_ptr netManager() const;
    QString bittrexApiKey() const;
    QString bittrexApiSecretKey() const;
};
typedef TradeApi* TradeApi_ptr;

#endif // TRADEAPI_H
