#include "util.h"

#include <QEventLoop>

void waitForSignal(QObject_ptr sender, const char *signal)
{
    QEventLoop loop;
    loop.connect(sender, signal, SLOT(quit()));
    loop.exec();
}
