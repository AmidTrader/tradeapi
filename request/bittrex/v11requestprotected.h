#ifndef BITTREX_V11REQUESTPROTECTED_H
#define BITTREX_V11REQUESTPROTECTED_H

#include "v11requestbase.h"

class BittrexV11RequestProtected: public BittrexV11RequestBase
{
protected:
    void setQueries(QUrl &url) const override;
    void setHeaders(QNetworkRequest &request) const override;

private:
    QByteArray makeApiSignKey(const QUrl &url) const;
};

#endif // BITTREX_V11REQUESTPROTECTED_H
