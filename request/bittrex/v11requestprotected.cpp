#include "v11requestprotected.h"

#include <QUrlQuery>
#include <QNetworkRequest>
#include <QMessageAuthenticationCode>

#include "tradeapi.h"

void BittrexV11RequestProtected::setQueries(QUrl &url) const
{
    BittrexV11RequestBase::setQueries(url);

    QUrlQuery query(url.query());
    query.addQueryItem("nonce", QString::number(QDateTime::currentDateTimeUtc().toTime_t()));
    query.addQueryItem("apikey", TradeApi::instance()->bittrexApiKey());

    url.setQuery(query);
}

void BittrexV11RequestProtected::setHeaders(QNetworkRequest &request) const
{
    BittrexV11RequestBase::setHeaders(request);

    request.setRawHeader( "apisign", makeApiSignKey(request.url()));
}

QByteArray BittrexV11RequestProtected::makeApiSignKey(const QUrl &url) const
{
    QMessageAuthenticationCode code(QCryptographicHash::Sha512);
    code.setKey(TradeApi::instance()->bittrexApiSecretKey().toUtf8());
    code.addData(url.toString().toUtf8());

    return code.result().toHex();
}
