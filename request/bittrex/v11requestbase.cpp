#include "v11requestbase.h"

#include <QUrlQuery>
#include <QDateTime>

#include "request/type.h"


const QString &BittrexV11RequestBase::apiUrl() const
{
    return API_V11_URL;
}
