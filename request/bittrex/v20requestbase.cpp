#include "v20requestbase.h"

#include <QUrlQuery>
#include <QDateTime>

#include "request/type.h"

const QString &BittrexV20RequestBase::apiUrl() const
{
    return API_V20_URL;
}

void BittrexV20RequestBase::setQueries(QUrl &url) const
{
    QUrlQuery query(url.query());
    query.addQueryItem("nonce", QString::number(QDateTime::currentDateTimeUtc().toTime_t()));

    url.setQuery(query);
}
