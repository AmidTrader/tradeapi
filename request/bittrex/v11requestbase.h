#ifndef BITTREX_V11REQUESTBASE_H
#define BITTREX_V11REQUESTBASE_H

#include "request/base.h"

class BittrexV11RequestBase: public ITradeApiRequestBase
{
protected:
    const QString &apiUrl() const override;

};

#endif // BITTREX_V11REQUESTBASE_H
