#ifndef REQUEST_V20REQUESTBASE_H
#define REQUEST_V20REQUESTBASE_H

#include "request/base.h"

class BittrexV20RequestBase: public ITradeApiRequestBase
{
protected:
    const QString &apiUrl() const override;
    void setQueries(QUrl &url) const override;
};

#endif // REQUEST_V20REQUESTBASE_H
