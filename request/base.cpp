#include "base.h"

#include <QNetworkReply>

#include "response/bittrex/base.h"
#include "util.h"

#include "tradeapi.h"

ITradeApiRequest::ITradeApiRequest(QObject_ptr parent)
    : QObject(parent)
{}

ITradeApiRequestBase::ITradeApiRequestBase(QObject_ptr parent)
    : ITradeApiRequest(parent)
{}

void ITradeApiRequestBase::execute(const QString &uri, IResponse_ptr response)
{
    QNetworkRequest request;

    setUrl(request, uri);
    setHeaders(request);
    QNetworkReply_ptr reply = executeRequest(request);
#ifndef QT_NO_OPENSSL
    setSslConfiguration(request, reply);
#endif
    processResponse(reply, response);

    delete reply;
}

void ITradeApiRequestBase::setUrl(QNetworkRequest &request,
                                  const QString &uri) const
{
    QUrl url = request.url();
    url.setUrl(QString("%1%2").arg(apiUrl()).arg(uri));
    setQueries(url);
    request.setUrl(url);
}

void ITradeApiRequestBase::setQueries(QUrl &url) const
{
    Q_UNUSED(url);
}

void ITradeApiRequestBase::setHeaders(QNetworkRequest &request) const
{
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
}

QNetworkReply_ptr ITradeApiRequestBase::executeRequest(const QNetworkRequest &request) const
{
    qDebug() << "[" << QDateTime::currentDateTimeUtc().toString(Qt::DefaultLocaleShortDate)
             << "] >>" << request.url();
    return TradeApi::instance()->netManager()->get( request );
}

void ITradeApiRequestBase::processResponse(const QNetworkReply_ptr reply,
                                           IResponse_ptr response) const
{
    waitForSignal(reply, SIGNAL(finished()));

    QByteArray response_data = reply->readAll();
    qDebug() << "[" << QDateTime::currentDateTimeUtc().toString(Qt::DefaultLocaleShortDate)
             << "] <<" << response_data;

    response->deserialize(response_data);

    qDebug() << QString("[%1] << Success: %2, Message: %3")
                .arg(QDateTime::currentDateTimeUtc().toString(Qt::DefaultLocaleShortDate))
                .arg(response->resultBase()->success)
                .arg(response->resultBase()->message);
}

#ifndef QT_NO_OPENSSL
void ITradeApiRequestBase::setSslConfiguration(QNetworkRequest &request,
                                               QNetworkReply_ptr reply) const
{
    QSslConfiguration ssl_conf = QSslConfiguration::defaultConfiguration();
    ssl_conf.setProtocol( QSsl::TlsV1_2 );
    request.setSslConfiguration( ssl_conf );

    connect( reply, &QNetworkReply::encrypted,
             this, &ITradeApiRequestBase::catchSslEncryption );
    connect( reply, &QNetworkReply::sslErrors,
             this, &ITradeApiRequestBase::handleSslErrors );
}

void ITradeApiRequestBase::catchSslEncryption()
{
    qDebug() << "Conection encrypted";
}

void ITradeApiRequestBase::handleSslErrors(const QList<QSslError> &errors) const
{
    auto reply = qobject_cast<QNetworkReply *>( sender() );

    for (auto err: errors)
        qWarning() << "SSL error during connecting: " << err.errorString();

    qDebug() << "all errors are ignored";
    reply->ignoreSslErrors();
}
#endif
