#ifndef REQUEST_TYPE_H
#define REQUEST_TYPE_H

#include <QString>

const QString API_V11_URL = "https://bittrex.com/api/v1.1";
const QString API_V20_URL = "https://bittrex.com/api/v2.0";

#endif // REQUEST_TYPE_H
