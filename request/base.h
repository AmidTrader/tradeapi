#ifndef REQUEST_BASE_H
#define REQUEST_BASE_H

#include "util.h"
#include "response/bittrex/base.h"

class QNetworkRequest;
class QNetworkReply;
class QUrl;
#ifndef QT_NO_OPENSSL
class QSslError;
#endif

typedef QNetworkReply* QNetworkReply_ptr;


class ITradeApiRequest: public QObject
{
public:
    ITradeApiRequest(QObject_ptr parent = nullptr);
    virtual ~ITradeApiRequest() = default;

    virtual void execute(const QString &uri, IResponse_ptr response) = 0;

protected:
    virtual const QString &apiUrl() const = 0;

    virtual void setUrl(QNetworkRequest &request, const QString &uri) const = 0;
    virtual void setQueries(QUrl &url) const = 0;
    virtual void setHeaders(QNetworkRequest &request) const = 0;

    virtual QNetworkReply_ptr executeRequest(const QNetworkRequest&request) const = 0;
    virtual void processResponse( const QNetworkReply_ptr reply,
                                  IResponse_ptr response ) const = 0;
};


class ITradeApiRequestBase: public ITradeApiRequest
{
public:
    ITradeApiRequestBase(QObject_ptr parent = nullptr);
    void execute(const QString &uri, IResponse_ptr response) override;

protected:
    void setUrl(QNetworkRequest &request, const QString &uri) const override;
    void setQueries(QUrl &url) const override;
    void setHeaders(QNetworkRequest &request) const override;

    QNetworkReply_ptr executeRequest(const QNetworkRequest &request) const override;
    void processResponse( const QNetworkReply_ptr reply,
                          IResponse_ptr response ) const override;

#ifndef QT_NO_OPENSSL
    virtual void setSslConfiguration( QNetworkRequest &request,
                                      QNetworkReply_ptr reply ) const;
    virtual void catchSslEncryption();
    virtual void handleSslErrors(const QList<QSslError> &errors) const;
#endif
};



#endif //REQUEST_BASE_H
