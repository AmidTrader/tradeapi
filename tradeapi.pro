#-------------------------------------------------
#
# Project created by QtCreator 2017-12-17T14:27:37
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = tradeapi
TEMPLATE = lib
CONFIG += staticlib
CONFIG += object_parallel_to_source no_batch

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        tradeapi.cpp \
    request/base.cpp \
    util.cpp \
    request/bittrex/v11requestbase.cpp \
    request/bittrex/v11requestpublic.cpp \
    request/bittrex/v11requestprotected.cpp \
    request/bittrex/v20requestbase.cpp \
    request/bittrex/v20requestpublic.cpp \
    response/bittrex/balance.cpp \
    response/bittrex/base.cpp \
    response/bittrex/btcprice.cpp \
    response/bittrex/markets.cpp \
    response/bittrex/marketssummary.cpp \
    response/bittrex/orderbook.cpp \
    response/bittrex/type.cpp \
    response/bittrex/currency.cpp \
    response/bittrex/openorder.cpp \
    response/bittrex/orderhistory.cpp \
    response/bittrex/neworder.cpp

HEADERS += \
        tradeapi.h \
    request/base.h \
    request/type.h \
    util.h \
    request/bittrex/v11requestbase.h \
    request/bittrex/v11requestpublic.h \
    request/bittrex/v11requestprotected.h \
    request/bittrex/v20requestbase.h \
    request/bittrex/v20requestpublic.h \
    response/bittrex/base.h \
    response/bittrex/balance.h \
    response/bittrex/btcprice.h \
    response/bittrex/markets.h \
    response/bittrex/marketssummary.h \
    response/bittrex/orderbook.h \
    response/bittrex/type.h \
    response/bittrex/currency.h \
    response/bittrex/openorder.h \
    response/bittrex/orderhistory.h \
    response/bittrex/neworder.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
