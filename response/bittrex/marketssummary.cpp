#include "marketssummary.h"

#include <QJsonObject>
#include <QJsonArray>

void ResponseMarketsSummary::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    if (value.isObject()) {
        result.add(deserializeOne(value.toObject()));
    } else if (value.isArray()) {
        for(const auto &item : value.toArray())
            result.add(deserializeOne(item.toObject()));
    }
}

MarketSummaryInfo ResponseMarketsSummary::deserializeOne(const QJsonObject &object)
{
    MarketSummaryInfo i;
    i.marketName = object[fieldName(MarketSummaryField::MarketName)].toString();
    i.high = object[fieldName(MarketSummaryField::High)].toDouble();
    i.low = object[fieldName(MarketSummaryField::Low)].toDouble();
    i.volume = object[fieldName(MarketSummaryField::Volume)].toDouble();
    i.last = object[fieldName(MarketSummaryField::Last)].toDouble();
    i.baseVolume = object[fieldName(MarketSummaryField::BaseVolume)].toDouble();
    i.timeStamp = QDateTime::fromString(
                    object[fieldName(MarketSummaryField::TimeStamp)].toString(),
                    Qt::ISODate);
    i.bid = object[fieldName(MarketSummaryField::Bid)].toDouble();
    i.ask = object[fieldName(MarketSummaryField::Ask)].toDouble();
    i.openBuyOrders = object[fieldName(MarketSummaryField::OpenBuyOrders)].toDouble();
    i.openSellOrders = object[fieldName(MarketSummaryField::OpenSellOrders)].toDouble();
    i.prevDay = object[fieldName(MarketSummaryField::PrevDay)].toDouble();
    i.created = QDateTime::fromString(
                    object[fieldName(MarketSummaryField::Created)].toString(),
                    Qt::ISODate);
    i.displayMarketName = object[fieldName(MarketSummaryField::DisplayMarketName)].toString();

    return i;
}
