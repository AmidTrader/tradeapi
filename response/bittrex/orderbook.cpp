#include "orderbook.h"

#include <QJsonObject>
#include <QJsonArray>

void ResponseOrderbook::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    for(const auto &buyItem : value.toObject()["buy"].toArray())
        result.buy.emplace_back(deserializeOne(buyItem.toObject()));
    for(const auto &sellItem : value.toObject()["sell"].toArray())
        result.sell.emplace_back(deserializeOne(sellItem.toObject()));
}

OrderBookInfo ResponseOrderbook::deserializeOne(const QJsonObject &object)
{
    OrderBookInfo i;
    i.quantity = object[fieldName(OrderBookField::Quantity)].toDouble();
    i.rate = object[fieldName(OrderBookField::Rate)].toDouble();

    return i;
}
