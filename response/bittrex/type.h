#ifndef RESPONSE_TYPE_H
#define RESPONSE_TYPE_H

#include <map>
#include <vector>
#include <QString>
#include <QUuid>
#include <QDateTime>

enum class BaseField { Success = 0, Message, Result };

enum class BalanceField { Currency = 0, Balance, Available, Pending,
                          CryptoAddress, Requested, Uuid };

enum class OrderBookField { Quantity = 0, Rate };

enum class MarketField { MarketCurrency = 0, BaseCurrency, MarketCurrencyLong,
                         BaseCurrencyLong, MinTradeSize, MarketName, IsActive,
                         Created };

enum class MarketSummaryField { MarketName = 0, High, Low, Volume, Last,
                                BaseVolume, TimeStamp, Bid, Ask, OpenBuyOrders,
                                OpenSellOrders, PrevDay, Created,
                                DisplayMarketName };

enum class BtcPriceField { UpdateTime = 0, Rate };

enum class CurrencyField { Currency = 0, CurrencyLong, MinConfirmation, TxFee,
                           IsActive, CoinType, BaseAddress };

enum class OpenOrderField { Uuid = 0, OrderUuid, Exchange, OrderType, Quantity,
                            QuantityRemaining, Limit, CommissionPaid, Price,
                            PricePerUnit, Opened, Closed, CancelInitiated,
                            ImmediateOrCancel, IsConditional, Condition,
                            ConditionTarget };

enum class OrderHistoryField { OrderUuid = 0, Exchange, TimeStamp, OrderType,
                               Quantity, QuantityRemaining, Limit, Commission,
                               Price, PricePerUnit, ImmediateOrCancel,
                               IsConditional, Condition, ConditionTarget };

enum class NewOrderField { OrderUuid = 0, Market, Quantity, Rate };

const QString &fieldName(BaseField field);
const QString &fieldName(BalanceField field);
const QString &fieldName(OrderBookField field);
const QString &fieldName(MarketField field);
const QString &fieldName(MarketSummaryField field);
const QString &fieldName(BtcPriceField field);
const QString &fieldName(CurrencyField field);
const QString &fieldName(OpenOrderField field);
const QString &fieldName(OrderHistoryField field);
const QString &fieldName(NewOrderField field);


struct ResponseBaseInfo
{
    bool success = false;
    QString message;
};
typedef ResponseBaseInfo* ResponseBaseInfo_ptr;


struct BalanceInfo
{
    QString currency;
    double balance = 0.;
    double available = 0.;
    double pending = 0.;
    QString cryptoAddress;
    bool requested = false;
    QUuid Uuid;
};
class BalanceData: public std::map<std::string /* Currency */, BalanceInfo>,
        virtual public ResponseBaseInfo {};
class BalanceCollection: public std::vector<BalanceInfo>,
        virtual public ResponseBaseInfo {};


struct OrderBookInfo
{
    double quantity = 0.;
    double rate = 0.;
};
typedef std::vector<OrderBookInfo> OrderBookCollection;


struct OrderBookData: virtual public ResponseBaseInfo
{
    OrderBookCollection buy;
    OrderBookCollection sell;
};


struct MarketInfo
{
    QString marketCurrency;
    QString baseCurrency;
    QString marketCurrencyLong;
    QString baseCurrencyLong;
    double minTraderSize = 0.;
    QString marketName;
    bool active = false;
    QDateTime created;
};
typedef std::vector<MarketInfo> MarketCollection;
typedef std::vector<QString> BaseMarketCollection;

class MarketsData: virtual public ResponseBaseInfo
{
public:
    void add(const MarketInfo &info);
    BaseMarketCollection baseMarkets() const;
    const MarketInfo &getInfo(const QString &marketName) const;
    const MarketCollection &getMarkets(const QString &baseCurrencyName) const;

private:
    MarketCollection _data;
    std::map<std::string /* BaseMarketCurrency */, MarketCollection> _marketsByBase;
};


struct MarketSummaryInfo
{
    QString marketName;
    double high = 0.;
    double low = 0.;
    double volume = 0.;
    double last = 0.;
    double baseVolume = 0.;
    QDateTime timeStamp;
    double bid = 0.;
    double ask = 0.;
    int openBuyOrders = 0.;
    int openSellOrders = 0.;
    double prevDay = 0.;
    QDateTime created;
    QString displayMarketName;
};
typedef std::vector<MarketSummaryInfo> MarketSummaryCollection;

class MarketsSummaryData: virtual public ResponseBaseInfo
{
public:
    void add(const MarketSummaryInfo &info);
    const MarketSummaryInfo &getInfo(const QString &marketName) const;

private:
    MarketSummaryCollection _data;
};


struct BtcPriceInfo: virtual public ResponseBaseInfo
{
    QDateTime updateTime;
    double rate = 0.;

    MarketCollection _data;
};


struct CurrencyInfo
{
    QString currency;
    QString currencyLong;
    int minConfirmation = 0;
    double txFee = 0.;
    bool active = false;
    QString coinType;
    QString baseAddress;
};
class CurrencyCollection: public std::map<std::string /* Currency */, CurrencyInfo>,
        virtual public ResponseBaseInfo {};


struct OpenOrderInfo
{
    enum class OrderType {Sell = 0, Buy};
    static OrderType typeFromString(const QString &stringType);

    QUuid Uuid;
    QUuid orderUuid;
    QString exchange;
    OrderType type;
    double quantity = 0.;
    double quantityRemaining = 0.;
    double limit = 0.;
    double commissionPaid = 0.;
    double price = 0.;
    double pricePerUnit = 0.;
    QDateTime openDate;
    QDateTime closeDate;
    bool cancelInitiated = false;
    bool immediateOrCancel = false;
    bool conditional = false;
    QString condition;  // TODO:
    QString conditionTarget;  // TODO:

};
class OpenOrderCollection: public std::vector<OpenOrderInfo>,
        virtual public ResponseBaseInfo {};



struct OrderHistoryInfo
{
    enum class OrderType {Sell = 0, Buy};
    static OrderType typeFromString(const QString &stringType);

    QUuid orderUuid;
    QString exchange;
    QDateTime timeStamp;
    OrderType type;
    double limit = 0.;
    double quantity = 0.;
    double quantityRemaining = 0.;
    double commission = 0.;
    double price = 0.;
    double pricePerUnit = 0.;
    bool conditional = false;
    QString condition;  // TODO:
    QString conditionTarget;  // TODO:
    bool immediateOrCancel = false;

};
class OrderHistoryCollection: public std::vector<OrderHistoryInfo>,
        virtual public ResponseBaseInfo {};


struct NewOrderInfo: virtual public ResponseBaseInfo
{
    enum class OrderType {Sell = 0, Buy};
    QString requestUri() const;
    QString marketName() const;

//    For request
    QString baseCurrency;
    QString currency;
    OrderType type;
    double quantity = 0.;
    double price = 0.;

//    From response
    QUuid orderUuid;
};

#endif // RESPONSE_TYPE_H
