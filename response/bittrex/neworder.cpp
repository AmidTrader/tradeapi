#include "neworder.h"

#include <QJsonObject>

void ResponseNewOrder::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    result.orderUuid = value[fieldName(OpenOrderField::Uuid)].toString();
}
