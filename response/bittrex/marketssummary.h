#ifndef RESPONSE_MARKETSSUMMARY_H
#define RESPONSE_MARKETSSUMMARY_H

#include "base.h"
#include "type.h"

class QJsonObject;

class ResponseMarketsSummary
        : public IResponseBase<MarketsSummaryData>
{
public:
    void deserializeResult(const QJsonValue &value) override;

private:
    MarketSummaryInfo deserializeOne(const QJsonObject &object);
};

#endif // RESPONSE_MARKETSSUMMARY_H
