#ifndef RESPONSE_CURRENCY_H
#define RESPONSE_CURRENCY_H

#include "base.h"
#include "type.h"

class QJsonObject;


class ResponseCurrency: public IResponseBase<CurrencyCollection>
{
public:
    void deserializeResult(const QJsonValue &value) override;

private:
    CurrencyInfo deserializeOne(const QJsonObject &object);
};

#endif // RESPONSE_CURRENCY_H
