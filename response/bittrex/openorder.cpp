#include "openorder.h"

#include <QJsonObject>
#include <QJsonArray>

void ResponseOpenOrder::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    if (value.isObject()) {
        result.emplace_back(deserializeOne(value.toObject()));
    } else if (value.isArray()) {
        for(const auto &item : value.toArray())
            result.emplace_back(deserializeOne(item.toObject()));
    }
}

OpenOrderInfo ResponseOpenOrder::deserializeOne(const QJsonObject &object)
{
    OpenOrderInfo i;
    i.Uuid = object[fieldName(OpenOrderField::Uuid)].toString();
    i.orderUuid = object[fieldName(OpenOrderField::OrderUuid)].toString();
    i.exchange = object[fieldName(OpenOrderField::Exchange)].toString();
    i.type = i.typeFromString(object[fieldName(OpenOrderField::OrderType)].toString());
    i.quantity = object[fieldName(OpenOrderField::Quantity)].toDouble();
    i.quantityRemaining = object[fieldName(
                OpenOrderField::QuantityRemaining)].toDouble();
    i.limit = object[fieldName(OpenOrderField::Limit)].toDouble();
    i.commissionPaid = object[fieldName(
                OpenOrderField::CommissionPaid)].toDouble();
    i.price = object[fieldName(OpenOrderField::Price)].toDouble();
    i.pricePerUnit = object[fieldName(OpenOrderField::PricePerUnit)].toDouble();
    i.openDate = QDateTime::fromString(
                    object[fieldName(OpenOrderField::Opened)].toString(),
                    Qt::ISODate);
    i.closeDate = QDateTime::fromString(
                    object[fieldName(OpenOrderField::Closed)].toString(),
                    Qt::ISODate);
    i.cancelInitiated = object[fieldName(
                OpenOrderField::CancelInitiated)].toBool();
    i.immediateOrCancel = object[
            fieldName(OpenOrderField::ImmediateOrCancel)].toBool();
    i.conditional = object[fieldName(OpenOrderField::IsConditional)].toBool();
    i.condition = object[fieldName(OpenOrderField::Condition)].toString();
    i.conditionTarget = object[fieldName(
                OpenOrderField::ConditionTarget)].toString();

    return i;
}
