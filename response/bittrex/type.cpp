#include "type.h"

#include <map>

const QString &fieldName(BaseField field)
{
    static const std::map<BaseField, QString> fieldsNames{
        {BaseField::Success, "success"},
        {BaseField::Message, "message"},
        {BaseField::Result, "result"}
    };

    return fieldsNames.at(field);
}

const QString &fieldName(BalanceField field)
{
    static const std::map<BalanceField, QString> fieldsNames{
        {BalanceField::Currency, "Currency"},
        {BalanceField::Balance, "Balance"},
        {BalanceField::Available, "Available"},
        {BalanceField::Pending, "Pending"},
        {BalanceField::CryptoAddress, "CryptoAddress"},
        {BalanceField::Requested, "Requested"},
        {BalanceField::Uuid, "Uuid"}
    };

    return fieldsNames.at(field);
}

const QString &fieldName(OrderBookField field)
{
    static const std::map<OrderBookField, QString> fieldsNames{
        {OrderBookField::Quantity, "Quantity"},
        {OrderBookField::Rate, "Rate"}
    };

    return fieldsNames.at(field);
}

const QString &fieldName(MarketField field)
{
    static const std::map<MarketField, QString> fieldsNames{
        {MarketField::MarketCurrency, "MarketCurrency"},
        {MarketField::BaseCurrency, "BaseCurrency"},
        {MarketField::MarketCurrencyLong, "MarketCurrencyLong"},
        {MarketField::BaseCurrencyLong, "BaseCurrencyLong"},
        {MarketField::MinTradeSize, "MinTradeSize"},
        {MarketField::MarketName, "MarketName"},
        {MarketField::IsActive, "IsActive"},
        {MarketField::Created, "Created"}
    };

    return fieldsNames.at(field);
}

const QString &fieldName(MarketSummaryField field)
{
    static const std::map<MarketSummaryField, QString> fieldsNames{
        {MarketSummaryField::MarketName, "MarketName"},
        {MarketSummaryField::High, "High"},
        {MarketSummaryField::Low, "Low"},
        {MarketSummaryField::Volume, "Volume"},
        {MarketSummaryField::Last, "Last"},
        {MarketSummaryField::BaseVolume, "BaseVolume"},
        {MarketSummaryField::TimeStamp, "TimeStamp"},
        {MarketSummaryField::Bid, "Low"},
        {MarketSummaryField::Ask, "Volume"},
        {MarketSummaryField::OpenBuyOrders, "Last"},
        {MarketSummaryField::OpenSellOrders, "BaseVolume"},
        {MarketSummaryField::PrevDay, "TimeStamp"},
        {MarketSummaryField::Created, "Created"},
        {MarketSummaryField::DisplayMarketName, "DisplayMarketName"}
    };

    return fieldsNames.at(field);
}

const QString &fieldName(BtcPriceField field)
{
    static const std::map<BtcPriceField, QString> fieldsNames{
        {BtcPriceField::UpdateTime, "UpdateTime"},
        {BtcPriceField::Rate, "Rate"}
    };

    return fieldsNames.at(field);
}

const QString &fieldName(CurrencyField field)
{
    static const std::map<CurrencyField, QString> fieldsNames{
        {CurrencyField::Currency, "Currency"},
        {CurrencyField::CurrencyLong, "CurrencyLong"},
        {CurrencyField::MinConfirmation, "MinConfirmation"},
        {CurrencyField::TxFee, "TxFee"},
        {CurrencyField::IsActive, "IsActive"},
        {CurrencyField::CoinType, "CoinType"},
        {CurrencyField::BaseAddress, "BaseAddress"}
    };

    return fieldsNames.at(field);
}

const QString &fieldName(OpenOrderField field)
{
    static const std::map<OpenOrderField, QString> fieldsNames{
        {OpenOrderField::Uuid, "Uuid"},
        {OpenOrderField::OrderUuid, "OrderUuid"},
        {OpenOrderField::Exchange, "Exchange"},
        {OpenOrderField::OrderType, "OrderType"},
        {OpenOrderField::Quantity, "Quantity"},
        {OpenOrderField::QuantityRemaining, "QuantityRemaining"},
        {OpenOrderField::Limit, "Limit"},
        {OpenOrderField::CommissionPaid, "CommissionPaid"},
        {OpenOrderField::Price, "Price"},
        {OpenOrderField::PricePerUnit, "PricePerUnit"},
        {OpenOrderField::Opened, "Opened"},
        {OpenOrderField::Closed, "Closed"},
        {OpenOrderField::CancelInitiated, "CancelInitiated"},
        {OpenOrderField::ImmediateOrCancel, "ImmediateOrCancel"},
        {OpenOrderField::IsConditional, "IsConditional"},
        {OpenOrderField::Condition, "Condition"},
        {OpenOrderField::ConditionTarget, "ConditionTarget"},
    };

    return fieldsNames.at(field);
}

const QString &fieldName(OrderHistoryField field)
{
    static const std::map<OrderHistoryField, QString> fieldsNames{
        {OrderHistoryField::OrderUuid, "OrderUuid"},
        {OrderHistoryField::Exchange, "Exchange"},
        {OrderHistoryField::TimeStamp, "TimeStamp"},
        {OrderHistoryField::OrderType, "OrderType"},
        {OrderHistoryField::Limit, "Limit"},
        {OrderHistoryField::Quantity, "Quantity"},
        {OrderHistoryField::QuantityRemaining, "QuantityRemaining"},
        {OrderHistoryField::Commission, "Commission"},
        {OrderHistoryField::Price, "Price"},
        {OrderHistoryField::PricePerUnit, "PricePerUnit"},
        {OrderHistoryField::IsConditional, "IsConditional"},
        {OrderHistoryField::Condition, "Condition"},
        {OrderHistoryField::ConditionTarget, "ConditionTarget"},
        {OrderHistoryField::ImmediateOrCancel, "ImmediateOrCancel"},
    };

    return fieldsNames.at(field);
}

const QString &fieldName(NewOrderField field)
{
    static const std::map<NewOrderField, QString> fieldsNames{
        {NewOrderField::OrderUuid, "uuid"},
        {NewOrderField::Market, "market"},
        {NewOrderField::Quantity, "quantity"},
        {NewOrderField::Rate, "rate"}
    };

    return fieldsNames.at(field);
}

void MarketsData::add(const MarketInfo &info)
{
    const bool baseCurrencyPresent = _marketsByBase.find(info.baseCurrency.toStdString())
            != _marketsByBase.end();

    if (baseCurrencyPresent)
        _marketsByBase.at(info.baseCurrency.toStdString()).emplace_back(info);
    else
        _marketsByBase.emplace(std::make_pair<std::string, MarketCollection>(
                                   info.baseCurrency.toStdString(), {info}));

    _data.emplace_back(info);
}

BaseMarketCollection MarketsData::baseMarkets() const
{
    BaseMarketCollection result;
    for(const auto &market : _marketsByBase)
        result.emplace_back(QString::fromStdString(market.first));

    return result;
}

const MarketInfo &MarketsData::getInfo(const QString &marketName) const
{
    return *std::find_if(_data.begin(), _data.end(),
                         [&marketName](const MarketInfo& i){
        return i.marketName == marketName;
    });
}

const MarketCollection &MarketsData::getMarkets(const QString &baseCurrencyName) const
{
    return _marketsByBase.at(baseCurrencyName.toStdString());
}

void MarketsSummaryData::add(const MarketSummaryInfo &info)
{
    _data.emplace_back(info);
}

const MarketSummaryInfo &MarketsSummaryData::getInfo(const QString &marketName) const
{
    return *std::find_if(_data.begin(), _data.end(),
                         [&marketName](const MarketSummaryInfo& i){
        return i.marketName == marketName;
    });
}

OpenOrderInfo::OrderType OpenOrderInfo::typeFromString(const QString &stringType)
{
    static const std::map<std::string, OrderType> fieldsNames{
        {"LIMIT_SELL", OrderType::Sell},
        {"LIMIT_BUY", OrderType::Buy}
    };

    return fieldsNames.at(stringType.toStdString());
}

OrderHistoryInfo::OrderType OrderHistoryInfo::typeFromString(const QString &stringType)
{
    static const std::map<std::string, OrderType> fieldsNames{
        {"LIMIT_SELL", OrderType::Sell},
        {"LIMIT_BUY", OrderType::Buy}
    };

    return fieldsNames.at(stringType.toStdString());
}

QString NewOrderInfo::requestUri() const
{
    static const std::map<OrderType, std::string> uris {
        {OrderType::Sell, "selllimit"},
        {OrderType::Buy, "buylimit"}
    };

    return QString::fromStdString(uris.at(type));
}

QString NewOrderInfo::marketName() const
{
    return QString("%1-%2").arg(baseCurrency).arg(currency);
}
