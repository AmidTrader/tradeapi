#include "balance.h"

#include <QJsonObject>
#include <QJsonArray>

BalanceInfo ResponseBalanceBase::deserializeOne(const QJsonObject &object)
{
    BalanceInfo i;
    i.currency = object[fieldName(BalanceField::Currency)].toString();
    i.balance = object[fieldName(BalanceField::Balance)].toDouble();
    i.available = object[fieldName(BalanceField::Available)].toDouble();
    i.pending = object[fieldName(BalanceField::Pending)].toDouble();
    i.cryptoAddress = object[fieldName(BalanceField::CryptoAddress)].toString();
    i.requested = object[fieldName(BalanceField::Requested)].toBool();
    i.Uuid = object[fieldName(BalanceField::Uuid)].toString();

    return i;
}

void ResponseBalanceData::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    if (value.isObject()) {
        BalanceInfo info(deserializeOne(value.toObject()));
        result[info.currency.toStdString()] = std::move(info);
    } else if (value.isArray()) {
        for(const auto &item : value.toArray()) {
            BalanceInfo info(deserializeOne(item.toObject()));
            result[info.currency.toStdString()] = std::move(info);
        }
    }
}

void ResponseBalanceCollection::deserializeResult(const QJsonValue &value)
{
    if (value.isObject()) {
        result.emplace_back(deserializeOne(value.toObject()));
    } else if (value.isArray()) {
        for(const auto &item : value.toArray())
            result.emplace_back(deserializeOne(item.toObject()));
    }
}
