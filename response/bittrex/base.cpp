#include "base.h"

ResponseBaseInfo_ptr IResponse::resultBase() const
{
    return _resultBase;
}

void IResponse::setResultBase(const ResponseBaseInfo_ptr resultBase)
{
    _resultBase = resultBase;
}
