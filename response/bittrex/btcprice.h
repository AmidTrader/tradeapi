#ifndef RESPONSE_BTCPRICE_H
#define RESPONSE_BTCPRICE_H

#include "base.h"
#include "type.h"

class QJsonObject;

class ResponseBtcPrice: public IResponseBase<BtcPriceInfo>
{
public:
    void deserializeResult(const QJsonValue &value) override;
};

#endif // RESPONSE_BTCPRICE_H
