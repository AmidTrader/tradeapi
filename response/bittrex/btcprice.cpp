#include "btcprice.h"

void ResponseBtcPrice::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    result.updateTime = QDateTime::fromString(
                value.toObject()["time"].toObject()["updatedISO"].toString(),
            Qt::ISODate);
    result.rate = value.toObject()["bpi"]
            .toObject()["USD"].toObject()["rate_float"].toDouble();
}
