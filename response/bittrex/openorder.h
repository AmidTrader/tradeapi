#ifndef RESPONSE_OPENOERDER_H
#define RESPONSE_OPENOERDER_H

#include "base.h"
#include "type.h"

class QJsonObject;

class ResponseOpenOrder: public IResponseBase<OpenOrderCollection>
{
public:
    void deserializeResult(const QJsonValue &value) override;

private:
    OpenOrderInfo deserializeOne(const QJsonObject &object);
};

#endif // RESPONSE_OPENOERDER_H
