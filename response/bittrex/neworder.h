#ifndef RESPONSE_NEWOERDER_H
#define RESPONSE_NEWOERDER_H

#include "base.h"
#include "type.h"

class QJsonObject;

class ResponseNewOrder: public IResponseBase<NewOrderInfo>
{
public:
    void deserializeResult(const QJsonValue &value) override;
};

#endif // RESPONSE_NEWOERDER_H
