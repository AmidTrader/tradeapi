#include "currency.h"

#include <QJsonObject>
#include <QJsonArray>

void ResponseCurrency::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    if (value.isObject()) {
        result[value.toObject()[fieldName(CurrencyField::Currency)].toString().toStdString()]
                = deserializeOne(value.toObject());
    } else if (value.isArray()) {
        for(const auto &item : value.toArray()) {
            result[item.toObject()[fieldName(CurrencyField::Currency)].toString().toStdString()]
                    = deserializeOne(item.toObject());
        }
    }
}

CurrencyInfo ResponseCurrency::deserializeOne(const QJsonObject &object)
{
    CurrencyInfo i;
    i.currency = object[fieldName(CurrencyField::Currency)].toString();
    i.currencyLong = object[fieldName(CurrencyField::CurrencyLong)].toString();
    i.minConfirmation = object[fieldName(CurrencyField::MinConfirmation)].toInt();
    i.txFee = object[fieldName(CurrencyField::TxFee)].toDouble();
    i.active = object[fieldName(CurrencyField::IsActive)].toBool();
    i.coinType = object[fieldName(CurrencyField::CoinType)].toString();
    i.baseAddress = object[fieldName(CurrencyField::BaseAddress)].toString();

    return i;
}
