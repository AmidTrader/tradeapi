#ifndef RESPONSE_BASE_H
#define RESPONSE_BASE_H

#include <vector>
#include <map>

#include <QString>

#include <QJsonDocument>
#include <QJsonObject>

#include "type.h"

class QJsonValue;


class IResponse
{
public:

    virtual ~IResponse() = default;

    virtual void deserialize(const QByteArray &jobject) = 0;
    ResponseBaseInfo_ptr resultBase() const;

protected:
    void setResultBase(const ResponseBaseInfo_ptr resultBase);
    virtual void deserializeResult(const QJsonValue &value) = 0;

private:
    ResponseBaseInfo_ptr _resultBase = nullptr;
};
typedef IResponse* IResponse_ptr;


template<class ResultType>
class IResponseBase: public IResponse
{
  public:
    ResultType result;

    IResponseBase()
    {
        setResultBase(dynamic_cast<ResponseBaseInfo_ptr>(&result));
    }

    void deserialize(const QByteArray &rawJson) override
    {
        const QJsonDocument jDoc(QJsonDocument::fromJson(rawJson));
        const QJsonObject jObject = jDoc.object();

        if (jObject.contains(fieldName(BaseField::Success)))
            result.success = jObject[fieldName(BaseField::Success)].toBool();
        if (jObject.contains(fieldName(BaseField::Message)))
            result.message = jObject[fieldName(BaseField::Message)].toString();
        if (jObject.contains(fieldName(BaseField::Result)))
            deserializeResult(jObject[fieldName(BaseField::Result)]);
    }

    void deserializeResult(const QJsonValue &value) override
    {
        Q_UNUSED(value);
        if (!result.success)
            return;
    }
};


#endif // RESPONSE_BASE_H
