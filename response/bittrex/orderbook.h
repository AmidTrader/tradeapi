#ifndef ORDERBOOK_H
#define ORDERBOOK_H

#include "base.h"
#include "type.h"

class QJsonObject;

class ResponseOrderbook: public IResponseBase<OrderBookData>
{
public:
    void deserializeResult(const QJsonValue &value) override;

private:
    OrderBookInfo deserializeOne(const QJsonObject &object);
};
#endif // ORDERBOOK_H
