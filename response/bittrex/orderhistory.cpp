#include "orderhistory.h"

#include <QJsonObject>
#include <QJsonArray>

void ResponseOrderHistory::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    if (value.isObject()) {
        result.emplace_back(deserializeOne(value.toObject()));
    } else if (value.isArray()) {
        for(const auto &item : value.toArray())
            result.emplace_back(deserializeOne(item.toObject()));
    }
}

OrderHistoryInfo ResponseOrderHistory::deserializeOne(const QJsonObject &object)
{
    OrderHistoryInfo i;
    i.orderUuid = object[fieldName(OrderHistoryField::OrderUuid)].toString();
    i.exchange = object[fieldName(OrderHistoryField::Exchange)].toString();
    i.timeStamp = QDateTime::fromString(
                    object[fieldName(OrderHistoryField::TimeStamp)].toString(),
                    Qt::ISODate);
    i.type = i.typeFromString(
                object[fieldName(OrderHistoryField::OrderType)].toString());
    i.limit = object[fieldName(OrderHistoryField::Limit)].toDouble();
    i.quantity = object[fieldName(OrderHistoryField::Quantity)].toDouble();
    i.quantityRemaining = object[fieldName(
                OrderHistoryField::QuantityRemaining)].toDouble();
    i.commission = object[fieldName(OrderHistoryField::Commission)].toDouble();
    i.price = object[fieldName(OrderHistoryField::Price)].toDouble();
    i.pricePerUnit = object[fieldName(OrderHistoryField::PricePerUnit)].toDouble();
    i.conditional = object[fieldName(OrderHistoryField::IsConditional)].toBool();
    i.condition = object[fieldName(OrderHistoryField::Condition)].toString();
    i.conditionTarget = object[fieldName(
                OrderHistoryField::ConditionTarget)].toString();
    i.immediateOrCancel = object[
            fieldName(OrderHistoryField::ImmediateOrCancel)].toBool();

    return i;
}
