#ifndef RESPONSE_MARKETS_H
#define RESPONSE_MARKETS_H

#include "base.h"
#include "type.h"

class QJsonObject;


class ResponseMarkets: public IResponseBase<MarketsData>
{
public:
    void deserializeResult(const QJsonValue &value) override;

private:
    MarketInfo deserializeOne(const QJsonObject &object);
};

#endif // RESPONSE_MARKETS_H
