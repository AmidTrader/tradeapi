#ifndef RESPONSE_BALANCE_H
#define RESPONSE_BALANCE_H

#include "base.h"
#include "type.h"

class QJsonObject;

class ResponseBalanceBase
{
protected:
    BalanceInfo deserializeOne(const QJsonObject &object);
};


class ResponseBalanceData
        : public IResponseBase<BalanceData>
        , public ResponseBalanceBase
{
public:
    void deserializeResult(const QJsonValue &value) override;
};


class ResponseBalanceCollection
        : public IResponseBase<BalanceCollection>
        , public ResponseBalanceBase
{
public:
    void deserializeResult(const QJsonValue &value) override;
};

#endif // RESPONSE_BALANCE_H
