#include "markets.h"

#include <QJsonObject>
#include <QJsonArray>

void ResponseMarkets::deserializeResult(const QJsonValue &value)
{
    IResponseBase::deserializeResult(value);

    if (value.isObject()) {
        result.add(deserializeOne(value.toObject()));
    } else if (value.isArray()) {
        for(const auto &item : value.toArray())
            result.add(deserializeOne(item.toObject()));
    }
}

MarketInfo ResponseMarkets::deserializeOne(const QJsonObject &object)
{
    MarketInfo i;
    i.marketCurrency = object[fieldName(MarketField::MarketCurrency)].toString();
    i.baseCurrency = object[fieldName(MarketField::BaseCurrency)].toString();
    i.marketCurrencyLong = object[fieldName(MarketField::MarketCurrencyLong)].toString();
    i.baseCurrencyLong = object[fieldName(MarketField::BaseCurrencyLong)].toString();
    i.minTraderSize = object[fieldName(MarketField::MinTradeSize)].toDouble();
    i.marketName = object[fieldName(MarketField::MarketName)].toString();
    i.active = object[fieldName(MarketField::IsActive)].toBool();
    i.created = QDateTime::fromString(
                object[fieldName(MarketField::Created)].toString(), Qt::ISODate);

    return i;
}
