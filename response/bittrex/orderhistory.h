#ifndef RESPONSE_ORDERHISTORY_H
#define RESPONSE_ORDERHISTORY_H

#include "base.h"
#include "type.h"

class QJsonObject;

class ResponseOrderHistory: public IResponseBase<OrderHistoryCollection>
{
public:
    void deserializeResult(const QJsonValue &value) override;

private:
    OrderHistoryInfo deserializeOne(const QJsonObject &object);
};

#endif // RESPONSE_ORDERHISTORY_H
