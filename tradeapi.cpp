#include "tradeapi.h"

#include <QDateTime>
#include <QEventLoop>
#include <QNetworkReply>
#include <QUrlQuery>
#include <QMessageAuthenticationCode>

#include "util.h"

#include "request/bittrex/v11requestpublic.h"
#include "request/bittrex/v11requestprotected.h"
#include "request/bittrex/v20requestpublic.h"

#include "response/bittrex/balance.h"
#include "response/bittrex/orderbook.h"
#include "response/bittrex/markets.h"
#include "response/bittrex/marketssummary.h"
#include "response/bittrex/btcprice.h"
#include "response/bittrex/currency.h"
#include "response/bittrex/openorder.h"
#include "response/bittrex/orderhistory.h"
#include "response/bittrex/neworder.h"


TradeApi *TradeApi::_instance = nullptr;

TradeApi *TradeApi::instance()
{
    if (!_instance) {
        _instance = new TradeApi;
        qDebug() << "TradeApi inited.";
    }

    return _instance;
}

TradeApi::TradeApi()
    : _netManager(new QNetworkAccessManager(this))
{}

TradeApi::~TradeApi()
{
    delete _instance;
}

void TradeApi::setBittrexApiKey(const QString &key)
{
    _bittrexApiKey = key;
}

void TradeApi::setBittrexApiSecretKey(const QString &key)
{
    _bittrexApiSecretKey = key;
}

BalanceCollection TradeApi::getBalances()
{
    ResponseBalanceCollection response;
    BittrexV11RequestProtected request;
    request.execute("/account/getbalances", &response);

    return response.result;
}

OrderBookData TradeApi::getOrderBook(const QString &baseCurrency, const QString &currency)
{
    ResponseOrderbook response;
    BittrexV11RequestPublic request;
    request.execute(QString("/public/getorderbook?market=%1-%2&type=both")
               .arg(baseCurrency).arg(currency), &response);
    return response.result;
}

MarketsData TradeApi::getMarkets()
{
    ResponseMarkets response;
    BittrexV11RequestPublic request;
    request.execute("/public/getmarkets", &response);
    return response.result;
}

MarketsSummaryData TradeApi::getMarketsSummary()
{
    ResponseMarketsSummary response;
    BittrexV11RequestPublic request;
    request.execute("/public/getmarketsummaries", &response);
    return response.result;
}

BtcPriceInfo TradeApi::getBtcPrice()
{
    ResponseBtcPrice response;
    BittrexV20RequestPublic request;
    request.execute("/pub/currencies/GetBTCPrice", &response);

    return response.result;
}

CurrencyCollection TradeApi::getCurrencies()
{
    ResponseCurrency response;
    BittrexV11RequestPublic request;
    request.execute("/public/getcurrencies", &response);

    return response.result;
}

OpenOrderCollection TradeApi::getOpenOrders(const QString &marketName)
{
    ResponseOpenOrder response;
    BittrexV11RequestProtected request;
    request.execute(QString("/market/getopenorders%1")
                    .arg(marketName.isEmpty() ? "" : "?market=" + marketName),
                    &response);

    return response.result;
}

OrderHistoryCollection TradeApi::getOrdersHistory(const QString &marketName)
{
    ResponseOrderHistory response;
    BittrexV11RequestProtected request;
    request.execute(QString("/account/getorderhistory%1")
                    .arg(marketName.isEmpty() ? "" : "?market=" + marketName),
                    &response);

    return response.result;
}

NewOrderInfo TradeApi::getCreateNewOrder(const NewOrderInfo &info)
{
    ResponseNewOrder response;
    response.result = info;
    BittrexV11RequestProtected request;
    request.execute(QString("/market/%1?%2=%3&%4=%5&%6=%7")
                    .arg(info.requestUri())
                    .arg(fieldName(NewOrderField::Market)).arg(info.marketName())
                    .arg(fieldName(NewOrderField::Quantity)).arg(info.quantity)
                    .arg(fieldName(NewOrderField::Rate)).arg(info.price)
                    , &response);

    return response.result;
}

QNetworkAccessManager_ptr TradeApi::netManager() const
{
    return _netManager;
}

QString TradeApi::bittrexApiKey() const
{
    return _bittrexApiKey;
}

QString TradeApi::bittrexApiSecretKey() const
{
    return _bittrexApiSecretKey;
}
