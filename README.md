# TradeApi
Qt library for crypto exchange APIs
===================================
### Currently supported exchanges:
- Bittrex.com
	- version 1.1
		-  /public/getmarkets
		-  /public/getmarketsummaries
		-  /public/getorderbook?market
		-  /public/getcurrencies
		-  /market/getopenorders
		-  /market/buylimit
		-  /market/selllimit
		-  /account/getbalances
		-  /account/getorderhistory
	-  version 2.0
		- /pub/currencies/GetBTCPrice

### Usage Example:
**Folders structure:**
```
root_folder
|___ app
    |___ your_app.pro
    |___ main.cpp
    |___ ...
|___ tradeapi
    |___ tradeapi.h
    |___ tradeapi.cpp
    |___ tradeapi.pro
    |___ ...
```

**your_app.pro file:**
```C++
# ...
CONFIG += c++11

SOURCES += main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../tradeapi/release/ -ltradeapi
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../tradeapi/debug/ -ltradeapi
else:unix: LIBS += -L$$PWD/../tradeapi/ -ltradeapi

INCLUDEPATH += $$PWD/../tradeapi/ 
DEPENDPATH += $$PWD/../tradeapi/

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../tradeapi/release/libtradeapi.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../tradeapi/debug/libtradeapi.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../tradeapi/release/tradeapi.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../tradeapi/debug/tradeapi.lib
else:unix: PRE_TARGETDEPS += $$PWD/../tradeapi/libtradeapi.a
# ...
```
**main.cpp:**
```C++
#include <QGuiApplication>
#include <QDebug>

#include "tradeapi.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    
    BtcPriceInfo btcPrice = TradeApi::instance()->getBtcPrice();
    
    qDebug() << QString("Currently (%1), 1 BTC = $%2")
                .arg(btcPrice.updateTime.toLocalTime()
                     .toString(Qt::DefaultLocaleShortDate)).arg(btcPrice.rate);

    return app.exec();
}
```

**Debug Output:**
```
Debugging starts
TradeApi inited.
Conection encrypted
[ "02.01.2018 11:01" ] >> QUrl("https://bittrex.com/api/v2.0/pub/currencies/GetBTCPrice?nonce=1514890899")
[ "02.01.2018 11:01" ] << "{\"success\":true,\"message\":\"\",\"result\":{\"time\":{\"updated\":\"Jan 2, 2018 11:01:00 UTC\",\"updatedISO\":\"2018-01-02T11:01:00+00:00\",\"updateduk\":\"Jan 2, 2018 at 11:01 GMT\"},\"disclaimer\":\"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org\",\"bpi\":{\"USD\":{\"code\":\"USD\",\"rate\":\"13,689.3325\",\"description\":\"United States Dollar\",\"rate_float\":13689.3325}}}}"
[ "02.01.2018 11:01" ] << Success: true , Message: ""
"Currently (02.01.2018 13:01), 1 BTC = $13689.3"
```